(function (global) {
	var investigateImageViewModel,
		app = global.app = global.app || {};
	
	function getMousePos(canvas, evt) {
		var rect = canvas.getBoundingClientRect();
		
		/* var html='';
		for (var key in evt) {
			html += key + '  ' + evt[key] + '<br />';
		}*/
		//$('#output').html ( evt.x.client );
		return {
			x: ((evt.x.client - rect.left)/rect.width)*100,
			y: ((evt.y.client - rect.top)/rect.height)*100
		};
			/*x: evt.clientX - rect.left,
			y: evt.clientY - rect.top*/
	}
	
	function createId() {
	
		var d = new Date(),
			sep = '_',
			h = d.getHours(),
			m = d.getMinutes(),
			s = d.getSeconds(),
			ss = d.getMilliseconds(),
			arr = [h,m,s,ss];

		return (arr.join(sep));
								
	}
	
	investigateImageViewModel = kendo.data.ObservableObject.extend({
		onViewInit: function () {		},
		onViewShow: function () {
			app.newStory.proceed({flag:true});
			
			var table = app.db.unpublishedBooks;
			table.orderBy('id').first(function(firstRecord) {
				var img = (firstRecord['image']);
				//$('#croppedImage img').attr('src', img);
				app.system.view().content.find('iron-image').attr('src', img);
			});
			app.newStory.progress(4);
			app.investigateImage.viewModel.shapeToolbar.init();
		},
		shapeToolbar: {
			init: function() {
				var $shapesToolbar = $("#toolbar-shapes");
				if (!$shapesToolbar.hasClass('initialised')) {
					$shapesToolbar.kendoTouch({
						tap: function(e) {
							app.investigateImage.viewModel.set("shapeToolbar.selectedItem", e.touch.initialTouch.getAttribute('data-icon'))
						}
					}).addClass('initialised');

					var pinWrap = document.getElementById('canvas-wrap'),
						overlay = document.getElementById('pinWrap'),
						pins = [];

					//pinWrap.setAttribute('style','width:' + app.settings.canvasDimensions().x + 'px; height:' + app.settings.canvasDimensions().y + 'px');
					if (!$(overlay).hasClass('initialised')) {
						$(overlay).kendoTouch({
							tap: function (e) {
								var initialObj = e.touch.initialTouch;

								if (initialObj.nodeName === "LI") {
									var pinId = initialObj.getAttribute('id');
									for (var i=0; i<pins.length; i++) {
										if (pins[i].id === pinId) {
											pins.splice(i, 1);
											break;
										}
									}
									initialObj.remove();
								} else {
									var pos = getMousePos(overlay, e.touch),
										pin = document.createElement('LI'),
										pinType = app.investigateImage.viewModel.shapeToolbar.selectedItem,
										pinStyle = 'left:'+pos.x+'%; top:'+pos.y+'%;',
										pinId = createId();

									pin.setAttribute('style', pinStyle);
									pin.setAttribute('data-icon', pinType);
									pin.setAttribute('id', pinId);

									pins.push({'style': pinStyle, 'data-icon': pinType, id: pinId});
									overlay.appendChild(pin);
								}
								console.log (pins);
								app.investigateImage.viewModel.set('pins', JSON.stringify(pins));
							}
						}).addClass('initialised');
					}
				}
			},
			selectedItem: null
		},
		pinsVisible: true,
		togglePins: function() {
			
			var currentState = app.investigateImage.viewModel.pinsVisible,
				newState;
			if (currentState == true) {
				newState = false;
			} else {
				newState = true
			}
			app.investigateImage.viewModel.set('pinsVisible', newState);
		},
		clearPins: function() {
			app.investigateImage.viewModel.pins = [];
			document.getElementById('pinWrap').innerHTML = '';
		},
		pins: '',
		commitPins: function() {
			var that = this;
			//console.log (that.pins);
			app.newStory.addField_in_db({pins:that.pins});
		}
	});
	
	
	app.investigateImage = {
		viewModel: new investigateImageViewModel()
	};
	
	
	app.investigateImage.viewModel.bind("change", function(e) {
		//console.log (e.field);
		if (e.field === "pins") {
			var field = app.investigateImage.viewModel.pins;
			if (field.length > 0) {
				console.log ('adding pins');
				app.newStory.addField_in_db({pins:field});
			} else {
				app.newStory.addField_in_db({pins:''});
			}
		}
	});
})(window);
