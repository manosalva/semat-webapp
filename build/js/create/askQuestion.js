(function (global) {
	var askQuestionViewModel,
		app = global.app = global.app || {},
		navigator;


	function initAudio() {

	}
	navigator = global.navigator;
	navigator.getUserMedia = (
		navigator.getUserMedia ||
		navigator.webkitGetUserMedia ||
		navigator.mozGetUserMedia ||
		navigator.msGetUserMedia
	);

	var Context = global.AudioContext || global.webkitAudioContext;
	var context = new Context();

	// we need these variables for later use with the stop function
	var mediaStream;
	var rec;

	function record() {
		// ask for permission and start recording
		navigator.getUserMedia({
			audio: true
		}, function (localMediaStream) {
			console.log ('trying');
			mediaStream = localMediaStream;

			// create a stream source to pass to Recorder.js
			var mediaStreamSource = context.createMediaStreamSource(localMediaStream);

			// create new instance of Recorder.js using the mediaStreamSource
			rec = new Recorder(mediaStreamSource, {
				// pass the path to recorderWorker.js file here
				//workerPath: '/vendor/js/recorderWorker.js'
			});

			// start recording
			rec.record();
		}, function (err) {
			console.log('Browser not supported');
			document.getElementById('record').setAttribute('style', 'display:none');
		});
	}

	function stop() {
		// stop the media stream
		mediaStream.stop();

		// stop Recorder.js
		rec.stop();

		// export it to WAV
		rec.exportWAV(function (e) {
			rec.clear();
			//Recorder.forceDownload(e, "filename.wav");
			
			var url = (window.URL || window.webkitURL).createObjectURL(e);
			console.log (url);
			app.newStory.addField_in_db({audio: url}, true);
			 // create a new request and send it via the objectUrl
			 /*var request = new XMLHttpRequest();
			 request.open("GET", url, true);
			 request.responseType = blob;
			 request.onload = function() {
			   // send the blob somewhere else or handle it here
			   // use request.response
			 }
			 request.send();*/
		});
	}
	
	function myCallback(blob){
	  
		 // Matt actually uses this line when he creates Recorder.forceDownload()
		 var url = (window.URL || window.webkitURL).createObjectURL(blob);

		 // create a new request and send it via the objectUrl
		 /*var request = new XMLHttpRequest();
		 request.open("GET", url, true);
		 request.responseType = blob;
		 request.onload = function(){
		   // send the blob somewhere else or handle it here
		   // use request.response
		 }
		 request.send();*/
	}


	askQuestionViewModel = kendo.data.ObservableObject.extend({
		questionText: '',
		questionTextSet: false,
		title: 'x',
		onViewInit: function () {
			console.log('audio init');
			initAudio();
		},
		onViewShow: function (e) {
			console.log('show');
			var that = app.askQuestion.viewModel,
				table = app.db.unpublishedBooks;
			table.orderBy('id').first(function (firstRecord) {
				that.set('questionText', firstRecord['questionText']);
				that.set('title', firstRecord['title']);
				app.system.view().content.find('iron-image').attr('src', firstRecord['image']);
			});
			app.newStory.progress(5);
			that.setChangeBindings(e);
		},
		setChangeBindings: function(e) {
			var view = app.system.view().content;
			if (view.attr('data-keypressbound') != 'true') {
				$('.nextBinding').on('keyup', function(clicked) {
					var input = clicked.target.innerHTML;
					if (input.length > 4) {
						if (app.askQuestion.viewModel.questionTextSet == false) {// this means that the title is being entered from zero length. This prevents calling the proceed function again and again
							app.askQuestion.viewModel.questionTextSet = true;
							app.newStory.addField_in_db({questionText: clicked.target.innerHTML});
							app.newStory.proceed({flag: true});
						}
					} else {
						app.askQuestion.viewModel.questionTextSet = false;
						app.newStory.proceed({flag: false});
					}
				});
				view.attr('data-keypressbound','true');
			}
		},
		soundRecorder: {
			toggleRecording: function () {
				
				//e = e.target;
				var e = document.getElementById('record');
				if (e.classList.contains("recording")) {
					stop();
					e.classList.remove("recording");
					
				} else {
					console.log ('recording');
					e.classList.add("recording");
					record();
				}
				
/*				if (e.classList.contains("recording")) {
					stop();
					e.classList.remove("recording");
					
				} else {
					console.log ('recording');
					e.classList.add("recording");
					record();
				}*/
			},
			play: function () {
				console.log('play');
				var that = this;
				app.storyRecorder._play.apply(that, arguments)
			},
			pause: function () {
				var that = this;
				app.storyRecorder._pause.apply(that, arguments)
			},
			stop: function () {
				var that = this;
				app.storyRecorder._stop.apply(that, arguments);
				app.askQuestion.viewModel.set('audioReady', true);
			}
		}
	});


	app.askQuestion = {
		viewModel: new askQuestionViewModel()
	};

	/*app.askQuestion.viewModel.bind("change", function(e) {
		if (e.field === "audioReady") {
			var audio = app.askQuestion.viewModel.audioReady;
			
			if (audio) {
				app.newStory.proceed({flag: true});
			} else {
				
			}
		}
	});*/


})(window);
