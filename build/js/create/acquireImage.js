(function (global) {
	var acquireImageViewModel,
		app = global.app = global.app || {};
	var errorCallback = function (e) {
		console.log('Reeeejected!', e);
	};

	function dataURItoBlob(dataURI) {
		// convert base64/URLEncoded data component to raw binary data held in a string
		var byteString;
		if (dataURI.split(',')[0].indexOf('base64') >= 0)
			byteString = atob(dataURI.split(',')[1]);
		else
			byteString = unescape(dataURI.split(',')[1]);

		// separate out the mime component
		var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

		// write the bytes of the string to a typed array
		var ia = new Uint8Array(byteString.length);
		for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
		}

		return new Blob([ia], {
			type: mimeString
		});
	}


	acquireImageViewModel = kendo.data.ObservableObject.extend({
		originalImage: null,
		viewmode: 'capture',
		onViewInit: function () {
			console.log('init image acq');

		},
		onViewShow: function () {
			app.newStory.proceed({
				flag: false
			});
			app.newStory.progress(2);
		},
		onImageLoaded: function() {
			app.system.navigate('#3');
		},
		_capturePhoto: function () {
			navigator.getUserMedia = navigator.getUserMedia ||
				navigator.webkitGetUserMedia ||
				navigator.mozGetUserMedia ||
				navigator.msGetUserMedia;

			var video = document.querySelector('video');
			var canvas = document.querySelector('canvas');
			var ctx = canvas.getContext('2d');
			var localMediaStream = null,
				that = this;

			function snapshot() {
				if (localMediaStream) {
					ctx.drawImage(video, 0, 0);
					// "image/webp" works in Chrome.
					// Other browsers will fall back to image/png.
					app.cropImage.viewModel.set('originalImage', canvas.toDataURL('image/webp'));
					app.acquireImage.viewModel.set('viewmode', 'readyToCrop');
					$("#imageCapture").data("kendoMobileModalView").close();
					app.acquireImage.viewModel.onImageLoaded();
				}
			}

			document.querySelector('#shootPhoto').addEventListener('click', snapshot, false);

			// Not showing vendor prefixes or code that works cross-browser.
			navigator.getUserMedia({
				video: true
			}, function (stream) {
				video.src = window.URL.createObjectURL(stream);
				localMediaStream = stream;
			}, errorCallback);

		},
		camModal: {
			onOpen: function () {
				//app.acquireImage.viewModel.set('viewmode', 'readyToCrop');
			},
			cancel: function () {
					console.log('cancel');
					$("#imageCapture").data("kendoMobileModalView").close();
					app.acquireImage.viewModel.set('viewmode', '');
				}
				/*,
							accept: function () {
								console.log('cancel');
								$("#imageCapture").data("kendoMobileModalView").close();
							}*/
		},
		libModal: {
			onOpen: function () {
				//app.acquireImage.viewModel.set('viewmode', 'readyToCrop');
			},
			cancel: function () {
					console.log('cancel');
					$("#imageCapture_from_album").data("kendoMobileModalView").close();
					app.acquireImage.viewModel.set('viewmode', '');
				}
				/*,
							accept: function () {
								console.log('cancel');
								$("#imageCapture_from_album").data("kendoMobileModalView").close();
								app.acquireImage.viewModel.createCrop();
							}*/
		},
		_loadPhotoFromDisk: function () {
			console.log('load from disk');
			var that = this;
			document.getElementById('files').addEventListener('change', handleFileSelect, false);
			var reader;
			var progress = document.querySelector('.percent');


			function errorHandler(evt) {
				switch (evt.target.error.code) {
				case evt.target.error.NOT_FOUND_ERR:
					alert('File Not Found!');
					break;
				case evt.target.error.NOT_READABLE_ERR:
					alert('File is not readable');
					break;
				case evt.target.error.ABORT_ERR:
					break; // noop
				default:
					alert('An error occurred reading this file.');
				};
			}

			function updateProgress(evt) {
				// evt is an ProgressEvent.
				if (evt.lengthComputable) {
					var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
					// Increase the progress bar length.
					if (percentLoaded < 100) {
						progress.style.width = percentLoaded + '%';
						progress.textContent = percentLoaded + '%';
					}
				}
			}

			function handleFileSelect(evt) {
				// Reset progress indicator on new file selection.
				console.log('file select');
				var file = evt.target.files[0];
				var imageType = /image.*/;

				if (file.type.match(imageType)) {
					progress.style.width = '0%';
					progress.textContent = '0%';

					reader = new FileReader();
					reader.onerror = errorHandler;
					reader.onprogress = updateProgress;

					reader.onloadstart = function (e) {
						document.getElementById('progress_bar').className = 'loading';
					};

					reader.onload = function (e) {
						//fileDisplayArea.innerHTML = "";

						// Create a new image.
						var img = new Image();
						// Set the img src property using the data URL.
						img.src = reader.result;

						// Add the image to the page.
						//fileDisplayArea.appendChild(img);

						progress.style.width = '100%';
						progress.textContent = '100%';
						setTimeout("document.getElementById('progress_bar').className='';", 2000);

						app.cropImage.viewModel.set('originalImage', reader.result);
						//app.acquireImage.viewModel.set('viewMode_imageReady', true);
						app.acquireImage.viewModel.onImageLoaded();
					}

					reader.readAsDataURL(file);
				} else {
					fileDisplayArea.innerHTML = "File not supported!";
				}

				//reader.readAsBinaryString(evt.target.files[0]);
			}

		},
		filmStrip: [],
		_getPhotoFromAlbum: function () {
			//app.acquireImage.viewModel.set('viewmode', 'readyToCrop');
			var dataSource = new kendo.data.DataSource({
				transport: {
					read: {
						url: "cfg/sampleImages.json",
						dataType: "json"
					}
				}
			});
			dataSource.fetch(function () {
				app.acquireImage.viewModel.set('filmStrip', this.data());
			});
		},

		_getPhoto: function (source) {
			var that = this;

		},
		selectFromFilmStrip: function (e) {
			var that = this;
			app.cropImage.viewModel.set('originalImage', e.item.find('img').attr('src'));
			that.set('filmStrip', []);
			//app.acquireImage.viewModel.set('viewmode', 'readyToCrop');
			$("#imageCapture_from_album").data("kendoMobileModalView").close();
			app.acquireImage.viewModel.onImageLoaded();
		},

		_onPhotoDataSuccess: function (imageData) {
			app.cropImage.viewModel.set('originalImage', "data:image/jpeg;base64," + imageData);
			app.acquireImage.viewModel.set('viewmode', 'readyToCrop');
		},

		_onPhotoURISuccess: function (imageURI) {

			app.cropImage.viewModel.set('originalImage', imageURI);
			//app.acquireImage.viewModel.set('viewmode', 'readyToCrop');
		},

		_onFail: function (message) {
			//app.acquireImage.viewModel.set('imageReady', false);
			alert(message);
		}

	});
	app.acquireImage = {
		viewModel: new acquireImageViewModel()
	};
})(window);
