(function (global) {
	var cropImageViewModel,
		app = global.app = global.app || {};


	cropImageViewModel = kendo.data.ObservableObject.extend({
		originalImage: null,
		viewmode: 'capture',
		onViewInit: function () {
			console.log('init image crop');

		},
		onViewShow: function () {
			app.newStory.proceed({
				flag: true
			});
			app.newStory.progress(3);
			setTimeout( app.cropImage.viewModel.createCrop, 1000);
		},
	
		createCrop: function () {
			var view = app.system.view().content;
			view.find('#originalImage').cropper({
				aspectRatio: 4 / 3,
				crop: function (data) {
					// Output the result data for cropping image.
				},
				zoomable: false,
				mouseWheelZoom: false,
				strict: true
			});
			app.newStory.proceed({
				flag: true
			});
		},
		finaliseCrop: function () {
			var view = app.system.view().content;
			var result = view.find('#originalImage').cropper("getCroppedCanvas", {
				width: app.settings.canvasDimensions().x,
				height: app.settings.canvasDimensions().y
			});
			view.find('#virtualCropper').html(result);

			var canvas = document.getElementById('virtualCropper').getElementsByTagName('canvas')[0];
			var uri = canvas.toDataURL();
			view.find('#originalImage').cropper('destroy');
			//console.log (blob);

			app.newStory.addField_in_db({
				image: uri
			}, true);//setting true will call the nextstep/proceed function
			console.log('finalising crop');
		}
	});
	app.cropImage = {
		viewModel: new cropImageViewModel()
	};
})(window);
