(function (global) {
	var titleViewModel,
		app = global.app = global.app || {};

	titleViewModel = kendo.data.ObservableObject.extend({
		onInit: function() {
			
		},
		onShow: function(e) {
			var table = app.db.unpublishedBooks,
				that = app.setTitle.viewModel;
			table.orderBy('id').first(function(firstRecord) {
				var title = (firstRecord['title']);
				if (title != null) {
					that.set('title', title);
					app.newStory.proceed({flag: true});
				} else {
					app.newStory.proceed({flag: false});
				}
				
			});
			app.newStory.progress(1);
			that.setChangeBindings(e);
		},
		setChangeBindings: function(e) {
			var view = app.system.view().content;
			if (view.attr('data-keypressbound') == 'false') {
				$('.nextBinding').on('keyup', function(clicked) {
					var input = clicked.target.value;
					if (input.length > 1) {
						if (app.setTitle.viewModel.title_set == false) {// this means that the title is being entered from zero length. This prevents calling the proceed function again and again
							app.setTitle.viewModel.title_set = true;
							app.newStory.proceed({flag: true});
						}
					} else {
						app.setTitle.viewModel.title_set = false;
						app.newStory.proceed({flag: false});
					}
				});
				view.attr('data-keypressbound','true');
			}
		},
		title: '',
		title_set:false,
		test: function() {alert('hi')}
	});
	
	app.setTitle = {
		viewModel: new titleViewModel()
	};
	app.setTitle.viewModel.bind("change", function(e) {
		if (e.field === "title") {
			var title = app.setTitle.viewModel.title;
			if (title.length > 1) {
				console.log (title.length);
				app.newStory.addField_in_db({title:title});
				//app.newStory.completedSteps = app.newStory.completedSteps + 1;
			} else {
				app.newStory.addField_in_db({title:''});
			}
		}
	});
})(window);
