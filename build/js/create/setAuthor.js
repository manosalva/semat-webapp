(function (global) {
	var authorViewModel,
		app = global.app = global.app || {};

	authorViewModel = kendo.data.ObservableObject.extend({
		onInit: function() {},
		onShow: function() {},
		author:''
	});
	
	app.setAuthor = {
		viewModel: new authorViewModel()
	};
	app.setAuthor.viewModel.bind("change", function(e) {
		if (e.field === "author") {
			app.newStory.addField_in_db({author:app.setAuthor.viewModel.author})
		}
	});
})(window);
