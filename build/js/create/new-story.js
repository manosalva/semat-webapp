(function (global) {
	var app = global.app = global.app || {},
		viewModel;

	/*	function NewStory() {}
		
		NewStory.prototype = {
			id: null,
			author: null,
			title:null,
			audio:null,
			image:null,
			pins:null,
			solution: {
				correctAnswer:null,
				incorrectAnswers: []
			}
		};*/

	/*function findExistingIDs() {
		var idArray = [];
		app.db.unpublishedBooks.orderBy('id').keys(function (ids) {
			
		});
	
		return ids;
	}*/

	function createNewStory_in_db() {
		app.db.unpublishedBooks.clear().then(function () {
			app.db.unpublishedBooks.add({});
		});
	}

	viewModel = kendo.data.ObservableObject.extend({
		onInit: function () {
			console.log('initialising new story');
			app.db.open().then(function () {
				createNewStory_in_db();
			});
			app.newStory.progressBar.init();
			//app.system.navigate('#1');
		},
		onShow: function() {
			 document.getElementById('create-layout').setAttribute('username','Patricia');
		},
		model: {
			id: null,
			author: null,
			title: null,
			audio: null,
			questionText: null,
			image: null,
			pins: null,
			correctAnswer: null,
			incorrectAnswers: []
		},
		reset: function () {
			var that = this;
			that.model = {
				author: null,
				title: null,
				audio: null,
				image: null,
				pins: null,
				solution: {
					correctAnswer: null,
					incorrectAnswers: []
				}
			}
		}/*,
		save_to_server: function () {
			var table = app.db.unpublishedBooks;
			table.orderBy('id').first(function (firstRecord) {
				var ajaxParams = {},
					fd = new FormData();

				for (var field in firstRecord) {
					if (field !== 'id') {
						fd.append(field, firstRecord[field]);
						ajaxParams[field] = firstRecord[field];
					}
				}

				if (ajaxParams['audio'] != '') {
					var xhr = new XMLHttpRequest();
					xhr.open('GET', ajaxParams['audio'], true);
					xhr.responseType = 'blob';
					xhr.onload = function (e) {
						if (this.status == 200) {
							ajaxParams.audioBlob = this.response;
							fd.append('audio', this.response);
							console.log (fd);
							saveToServer(fd);
						}
					};
					xhr.send();

				} else {
					saveToServer(ajaxParams)
				}


			});
		}*/
	});

	

	app.newStory = {
		viewModel: new viewModel(),
		currentStep:1,
		completedSteps:0,
		progress: function(activeStep, completedSteps) {
			if (activeStep) {
				app.newStory.currentStep = activeStep;
				app.newStory.progressBar.refresh();
			} else {
				return app.newStory.currentStep
			}
		},
		addField_in_db: function (params, proceed) {
			var table = app.db.unpublishedBooks;
			table.orderBy('id').first(function (firstRecord) {
				var proceedFn = function() {};
				if (proceed == true) proceedFn = app.newStory.proceed;
				table.update(firstRecord.id, params).then(proceedFn);
			});
		},
		getField_from_db: function (field) {
			var table = app.db.unpublishedBooks;
			table.orderBy('id').first(function (firstRecord) {
				console.log(firstRecord[field]);
				return firstRecord[field];
			});
		},
		updateProgress: function() {
			var that = app.newStory;
			
			
		},
		customProceedFn: function(){},
		proceed: function(params) {
			//console.log (params.flag);
			if (params && typeof params.flag !== 'undefined') {
				console.log ('here');
				var view = app.system.view(), flag = params.flag,
					button = view.footer.find('button'),
					$button = $(button).data('kendoMobileButton');

				if (flag == false) {
					console.log ('disable next');
					$button.enable(false);
				} else if (flag == true) {
					$button.enable();
					console.log ('enable next');
				}
			} else {
				var that = app.newStory;
				console.log ('next ' + that.currentStep);
				that.currentStep = that.currentStep + 1;
				/*if (that.completedSteps < that.currentStep) {
					$button.enable(false);
				}*/
				//var customFn = sessionStorage.getItem('customProceedFn');
				/*console.log (customFn);
				if (customFn != null) {
					that.customProceedFn = eval(customFn);
					that.customProceedFn();
					that.customProceedFn = function(){};
				}*/
				
				//app.system.navigate('/build/views/' + that.currentStep + '.html')
				app.system.navigate('#' + that.currentStep);
				//setTimeout(app.newStory.progressBar.refresh,1000);
			}
		},
		save_to_server: function () {
			var table = app.db.unpublishedBooks;
			table.orderBy('id').first(function (firstRecord) {
				var ajaxParams = {},
					fd = new FormData();

				for (var field in firstRecord) {
					if (field !== 'id') {
						fd.append(field, firstRecord[field]);
						ajaxParams[field] = firstRecord[field];
					}
				}
				
				saveToServer(ajaxParams)

				/*if (ajaxParams['audio'] != undefined) {
					console.log ('audio found ' + ajaxParams['audio']);
					var xhr = new XMLHttpRequest();
					xhr.open('GET', ajaxParams['audio']);
					xhr.responseType = 'blob';
					xhr.onload = function (e) {
						if (this.status == 200) {
							ajaxParams.audioBlob = this.response;
							fd.append('audio', this.response);
							console.log (fd);
							saveToServer(fd);
						}
					};
					xhr.send();

				} else {
					saveToServer(ajaxParams)
				}
*/

			});
		}

	}
	function saveToServer(data) {
		console.log (data);
		//var serData = JSON.stringify(data.serializeObject());
		console.log ('serData');
		$.ajax({
			url: app.path('story'),
			data: data,
			type: 'post'
			/*headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},*/
		}).done(function (msg) {
			alert("Data Saved: " + msg);
		});
	}
	



})(window);
