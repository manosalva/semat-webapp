(function (global) {
	var provideSolutionViewModel,
		app = global.app = global.app || {};
	
	function renderPinsToScreen(pins) {
		var parent = document.getElementById('provideSolution_pinWrap'),
			li = document.createElement('li'),
			i=0;
		
		for (i; i < pins.length; i++) {
			console.log (i);
			var li_ = li.cloneNode();
			li_.setAttribute('style', pins[i].style);
			li_.setAttribute('data-icon', pins[i]['data-icon']);
			
			console.log (li_);
			parent.appendChild(li_);
		}
	}

	provideSolutionViewModel = kendo.data.ObservableObject.extend({
		croppedImage: '/build/images/House_Boston.jpg',
		questionText: '',
		title: '',
		questionAudio: '',
		onViewInit: function() {
			
		},
		onViewShow: function() {
			app.provideSolution.viewModel.solution.init();

			var imgEl = document.querySelector('iron-image');
			//imgEl.setAttribute('src', 'http://localhost/semat_sample/' + request.detail.response.img);
			
			var that = app.provideSolution.viewModel;
				/*fetch = app.newStory.getField_from_db,
				img =  app.newStory.getField_from_db('image'),
				audio =  app.newStory.getField_from_db('title'),
				title =  app.newStory.getField_from_db('title'),
				questionText =  app.newStory.getField_from_db('questionText');*/
			
			var table = app.db.unpublishedBooks;
			table.orderBy('id').first(function (firstRecord) {
				that.set('questionText', firstRecord['questionText']);
				that.set('title', firstRecord['title']);
				
				app.system.view().content.find('iron-image').attr('src', firstRecord['image']);
				
				var pins = JSON.parse(firstRecord['pins']);
				renderPinsToScreen(pins);
			});
			app.newStory.progress(6);
		},
		complete: false,
		confirmChoices: function() {
			console.log ('saving');
			var that = this;
			
			app.newStory.addField_in_db({correctAnswer: that.correctAnswer});
		},
		solution: {
			init: function() {
				var that = this,
					el = $("#numericEntry");
				
				if (!el.hasClass('initialised')) {
					el.kendoTouch({
						tap: function (e) {
							var initialObj = e.touch.initialTouch;

							if (initialObj.nodeName === "LI") {
								that.enter (initialObj);
							}
						}
					}).addClass('initialised');
				}
			},
			polarity: function() {
				var num = app.provideSolution.viewModel.correctAnswer;
				if (num.charAt(0) === "-") {
					num = num.replace('-','');
				} else {
					num = "-" + num;
				}
				
				app.provideSolution.viewModel.set('correctAnswer',  num);
			},
			clear: function() {
				app.provideSolution.viewModel.set('correctAnswer',  '0');
			},
			enter: function(obj) {
				var fn = obj.getAttribute('data-fn'),
					that = this;
				if (fn === 'enter') {
					var prev = app.provideSolution.viewModel.correctAnswer,
					newNum;
					if ((obj.innerHTML === '.' && prev.indexOf('.') === -1) || obj.innerHTML !== '.') {
						newNum = prev + obj.innerHTML;
						app.provideSolution.viewModel.set('correctAnswer',  newNum);
					}
				} else {
					that[fn]();
				}
			}
		},
		correctAnswer:''
	});
	
	app.provideSolution = {
		viewModel: new provideSolutionViewModel()
	};
	
	app.provideSolution.viewModel.bind("change", function(e) {
		if (e.field === "correctAnswer") {
			var field = app.provideSolution.viewModel.correctAnswer;
			if (field.length > 1) {
				console.log (field.length);
				app.newStory.addField_in_db({correctAnswer:field});
				app.newStory.proceed({flag:true});
			} else {
				app.newStory.addField_in_db({correctAnswer:''});
				app.newStory.proceed({flag:false});
			}
		}
	});
})(window);
