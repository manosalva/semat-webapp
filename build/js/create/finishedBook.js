(function (global) {
	var finishedBookViewModel,
		app = global.app = global.app || {};

	finishedBookViewModel = kendo.data.ObservableObject.extend({
		onViewShow: function() {
			app.newStory.save_to_server();
		},
		author:'cecilia',
		title:'my story',
		pins:'',
		correctAnswer:'',
		incorrectAnswers:'',
		image:'',
		audio:'',
		questionText:'',
		onShow: function(e) {
			var table = app.db.unpublishedBooks,
				that = app.finishedBookViewModel.viewModel;
			table.orderBy('id').first(function(firstRecord) {
				that.set('title', firstRecord['title']);
				that.set('pins', firstRecord['pins']);
				that.set('correctAnswer', firstRecord['correctAnswer']);
				that.set('incorrectAnswers', firstRecord['incorrectAnswers']);
				that.set('image', firstRecord['image']);
				that.set('audio', firstRecord['audio']);
				that.set('questionText', firstRecord['questionText']);
			});

		}
	});
	
	app.finishedBook = {
		viewModel: new finishedBookViewModel()
	};
	
})(window);
