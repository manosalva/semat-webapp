(function (global) {
	var app = global.app = global.app || {};
	
	app.newStory.progressBar = {
		init: function () {
				
		},
		refresh: function() {
			var nav = app.system.view().header.find('nav');
			var progressNodes = nav.find('button'),
				currentProgress = app.newStory.progress(),
				currentStep = app.newStory.currentStep;
			
			console.log (currentProgress);
			for (var i=1; i<=progressNodes.length; i++) {
				//console.log (i,currentProgress);
				var node = progressNodes[i-1];
				node.setAttribute('data-active','');
				if (i === currentStep) {
					node.setAttribute('data-active','true');
				}
			}
		},
		clearSelection: function() {
			
		}
		/*redraw: function() {
			var that = this;
			$('#progressbar').data('kendoProgressBar').value(that.progress)
		},*/
		/*init: function(value) {
			var that = this,
				$el = $('#progressBar'),
				nodeData = [
					{view:'acquireImage', step:0},
					{view:'investigateImage', step:1},
					{view:'askQuestion', step:2},
					{view:'provideSolution', step:3}
				],
				initVal = value || that.progress || 0;
			
			console.log (initVal);
			
			var progressbar = $el.kendoProgressBar({
				type: "chunk",
				chunkCount: 4,
				min: 0,
				max: 4,
				value: initVal
			}).data("kendoProgressBar");
			
			$el.find('li').each( function(i) {
				this.setAttribute('data-target', nodeData[i].view)
			});
			
			
			$el.kendoTouch({
				tap: function(e) {
					var initialObj = e.touch.initialTouch,
						vm = app.provideSolution.viewModel,
						theClass = "k-state-selected";
					
					if (initialObj.nodeName === "LI") {
						var $node = $(initialObj);
						app.system.navigate('views/' + $node.attr('data-target') + '.html');
					}
					
				}
			})
		}*/
	};
})(window);
