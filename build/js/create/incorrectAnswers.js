(function (global) {
	var incorrectAnswersViewModel,
		app = global.app = global.app || {};

	function getRandomArbitrary(min, max) {
		return Math.random() * (max - min) + min;
	}

	function getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	function shuffle(array) {
		var currentIndex = array.length,
			temporaryValue, randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {

			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}

	function createRandomIncorrectChoices(base) {

		var total = 32,
			total_negInt = 0,
			total_posInt = 0,
			total_negFloat = 0,
			total_posFloat = 0,
			array = [],
			base = base.toString();
		console.log ('creating random ' + base);
		if (base.charAt(0) === "-" && base.indexOf('.') === -1) { //negative integer
			total_negInt = total_posInt = .5;
		}
		if (base.charAt(0) === "-" && base.indexOf('.') !== -1) { //negative float
			total_negInt = total_posInt = total_negFloat = total_posFloat = .25;
		}
		if (base.charAt(0) !== "-" && base.indexOf('.') !== -1) { //positive float
			total_posInt = total_posFloat = 0.5;
		}
		if (base.charAt(0) !== "-" && base.indexOf('.') === -1) { //positive integer
			total_posInt = 1;
		}
		console.log(total_negInt, total_posInt, total_negFloat, total_posFloat);

		for (var i = 0; i < total * total_posInt; i++) {
			array.push(getRandomInt(0, base * 1.5));
		}
		for (var i = 0; i < total * total_negInt; i++) {
			array.push(-1 * getRandomInt(0, base * 1.5));
		}
		for (var i = 0; i < total * total_posFloat; i++) {
			array.push(getRandomArbitrary(0, base * 1.5).toFixed(2));
		}
		for (var i = 0; i < total * total_negFloat; i++) {
			array.push(-1 * getRandomArbitrary(0, base * 1.5).toFixed(2));
		}
		var shuffledArray = shuffle(array);
		return (shuffledArray);
	}


	incorrectAnswersViewModel = kendo.data.ObservableObject.extend({
		onViewInit: function () {

		},
		onViewShow: function () {
			app.newStory.progress(7);
			var that = this;
			var table = app.db.unpublishedBooks;
			table.orderBy('id').first(function (firstRecord) {
				
				app.incorrectAnswers.viewModel.renderRandomChoices(firstRecord['correctAnswer']);
			});
			
			
		},
		complete: false,
		confirmChoices: function () {
			console.log('saving');
			var that = this;
			app.newStory.addField_in_db({
				wrongAnswers: that.incorrectAnswers_selected
			});
		},
		correctAnswer: '0',
		incorrectAnswers_selected: [],
		incorrectAnswers_random: [],
		renderRandomChoices: function (base) {
			var shuffledArray = createRandomIncorrectChoices(base);
			app.incorrectAnswers.viewModel.set('incorrectAnswers_random', shuffledArray);

			var $incorrectList = $('#incorrectAnswers_random');
			if (!$incorrectList.hasClass('initialised')) {
				$incorrectList.kendoTouch({
					tap: function (e) {
						var initialObj = e.touch.initialTouch,
							vm = app.incorrectAnswers.viewModel,
							theClass = "k-state-selected";

						if (initialObj.nodeName === "LI") {
							var $node = $(initialObj);
							if (vm.incorrectAnswers_selected.length < 3) {
								$node.toggleClass(theClass);
							} else {
								if ($node.hasClass(theClass)) {
									$node.removeClass(theClass);
								}
							}
							var selection = [];
							$incorrectList.find('.' + theClass).each(function (i, v) {
								selection.push(v.innerHTML);
							});
							vm.set('incorrectAnswers_selected', selection);

							if (app.incorrectAnswers.viewModel.incorrectAnswers_selected.length == 3) {
								app.newStory.proceed({flag:true})
							} else {
								app.newStory.proceed({flag:false})
							}
						}

					}
				}).addClass('initialised');
			}
		},
		enabled: false

	});


	app.incorrectAnswers = {
		viewModel: new incorrectAnswersViewModel()
	};
})(window);
