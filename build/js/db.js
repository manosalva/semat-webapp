(function (global) {
	var app = global.app = global.app || {};

	
	app.db = new Dexie(app.settings.dbName);
	app.db.version(1).stores({
		unpublishedBooks: "++id,author,title,date,image,audio,pins,correctAnswer,wrongAnswers"
	});
	
	

	/*
	app.storage = {
		openDb: function() {
			var db = app.db;
			db.version(1).stores({
				unpublishedBooks: "++id,author,title,date,image,audio,pins,correctAnswer,wrongAnswers"
			});
			db.open();
		},
		closeDb: function() {
			app.db.close();
		},
		init: function () {
			/*app.db = new Dexie(app.settings.dbName);
			var that = this;
			that.openDb();*/
			//app.db.clear();
			
			

			/*function createDb() {
				console.log ('creating db ' + app.settings.dbName);
				app.db = new Dexie(app.settings.dbName);
				var db = app.db;
				db.version(1).stores({
					friends: "++id,name,shoeSize"
				});
				db.open();
				//, date, image, audio, pins, correctAnswer, wrongAnswers
			}

			Dexie.getDatabaseNames(function (databaseNames) {
				var dbFound = false;
				if (databaseNames.length === 0) {
					// No databases at this origin as we know of.
					console.log("There are no databases at current origin. Try loading another sample and then go back to this page.");
					createDb();
				} else {
					// At least one database to dump
					dump(databaseNames);
				}
				function dump(databaseNames) {
					if (databaseNames.length > 0) {
						var db = new Dexie(databaseNames[0]);
						// Now, open database without specifying any version. This will make the database open any existing database and read its schema automatically.
						db.open().then(function () {
							if (db.name === app.settings.dbName){
								console.log ('db exists');
								app.db = new Dexie(databaseNames[0]);//declare as a global db
								dbFound = true;
							}
						}).finally(function () {
							db.close();
							dump(databaseNames.slice(1));
						});
					} else {
						if (!dbFound) createDb();
					}
				}
			});



		}
	};*/
	
})(window);
