(function (global) {
	//jan@borginteractive.com @ semat
	var app = global.app = global.app || {};
			//app.storyRecorder = new StoryRecorder();
			app.system = new kendo.mobile.Application( $(document.body), {
				initial: "#view_library"
			});
/*	kendo.data.binders.widget.keyPress = kendo.data.Binder.extend({
		init: function (element, bindings, options) {
			kendo.data.Binder.fn.init.call(this, element, bindings, options);
			var binding = this.bindings.keyPress;
			$(element.input).bind("keypress", function (e) {
				if (e.which == 13) {
					binding.get();
				}
			});
		},
		refresh: function () { }
	});*/
	
	app.settings= {
		dbName : 'SeMat'
	};
	app.settings.canvasDimensions = function() {
		var $window = $(window),
			width,
			height;
		
		if ( $window.width() > $window.height()) {
			width = $window.width(),
			height = $window.height();
		} else {
			height = $window.width(),
			width = $window.height();
		}

		return {
			x: Math.round(width*0.7),
			y: Math.round(height*0.7)
		};
	}
})(window);

	$.fn.serializeObject = function()
	{
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			console.log (a);
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
